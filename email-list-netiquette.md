** EMAIL LIST NETIQUETTE **

1. Emailing the list
To email the list, make sure you add the list address in the To or Cc fields. In some instances, Wallymer's list may be invite-only, in which case you won't be able to join and/or post without approval.

2. Replying / Bottom-posting
On our email list, we require that you bottom-post in reply to an email. Here's a real example from a conversation Slade had in reply to Greg KH on the Linux Kernel Mailing List [1]

-- 
On Mon, Aug 29, 2022, at 6:58 AM, Greg Kroah-Hartman wrote:
> This is the start of the stable review cycle for the 5.10.140 release.
> There are 86 patches in this series, all will be posted as a response
> to this one.  If anyone has any issues with these being applied, please
> let me know.
>
> Responses should be made by Wed, 31 Aug 2022 10:57:37 +0000.
> Anything received after that time might be too late.

5.10.140-rc1 compiled and booted with no errors or regressions on my x86_64 test system.

Tested-by: Slade Watkins <slade@sladewatkins.com>

Cheers, 
-srw
--

All bottom-posting [2] requires you do is scroll down, click/tap where you want to reply, reply to the specific parts of the conversation. It's basically like having a real conversation, by email, trimming out the parts that have *already been discussed.*

3. Trimming
Which brings us to Trimming. Please ensure you are _trimming_ the contents of your email. If someone else's reply isn't required for context, get rid of it. No walls of text. Please. [3] 

4. Footnotes
[1] https://lore.kernel.org/lkml/8d92c03a-67ca-4064-9e0c-0a19c2a87904@www.fastmail.com/
[2] https://en.wikipedia.org/wiki/Posting_style#Bottom-posting
[3] https://en.wikipedia.org/wiki/Posting_style#Trimming_and_reformatting
